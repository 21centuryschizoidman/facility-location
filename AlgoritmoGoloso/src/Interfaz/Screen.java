package Interfaz;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import com.google.gson.reflect.TypeToken;

import codigoNegocio.Cliente;
import codigoNegocio.EntidadToJSON;
import codigoNegocio.Solver;
import codigoNegocio.Sucursal;
import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.JLabel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class Screen extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	private Font font = new Font("Microsoft Sans Serif", Font.PLAIN, 14);
	private JButton play = new JButton("Mapa");
	private JButton mainMenu = new JButton("\u2190");
	
	private CardLayout layout = new CardLayout();
	
	private JPanel panel = new JPanel();
	private JPanel mapa = new JPanel();
	private JPanel menu = new JPanel(); 
	
	private Solver solver = new Solver();
	
	private JMapViewer map = new JMapViewer();
	private final JButton btnAbrirJsonCliente = new JButton("JSON Clientes");
	private final JButton btnAbrirJsonSucursal = new JButton("JSON Sucursales");
	private boolean ingresoJsonCliente = false;
	private boolean ingresoJsonSucursal = false;
	private JTextField cantSucursales;
	private List<MapMarker> mapMarkerList = new ArrayList<MapMarker>();
	private EntidadToJSON<Sucursal> leerSucursales;
	private EntidadToJSON<Cliente> leerClientes;
	private JTextField textoMapa;

	public Screen() {
	  	setForeground(Color.BLACK);
	  	setFont(new Font("Microsoft Sans Serif", Font.PLAIN, 12));
	  	setBackground(Color.BLACK);
	
	    panel.setLayout(layout);        
	    addButtons();
	
 	    ImageIcon img = new ImageIcon("res/Sucursal.png");
	    setIconImage(img.getImage());
	    setSize(684, 632);
	    setResizable(false);
	    setLocationRelativeTo(null);
	    setVisible(true);
	    setTitle("Distribuición de sucursales");
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    requestFocus();
	
	}
	
	private void addButtons() {
		play.setForeground(Color.WHITE);
		play.setBackground(Color.DARK_GRAY);
		play.setBounds(269, 490, 162, 36);
		play.addActionListener(this);
		play.setFont(font);
		mainMenu.setBackground(Color.DARK_GRAY);
		mainMenu.setForeground(Color.WHITE);
		mainMenu.setFont(new Font("Microsoft Sans Serif", Font.PLAIN, 20));
		mainMenu.setBounds(10, 10, 62, 24);
		mainMenu.addActionListener(this);
		map.setBounds(10, 41, 648, 538);
		map.setZoomContolsVisible(false);
		map.setMapMarkerList(mapMarkerList);
		mapa.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				map.resize(mapa.getWidth()-20,mapa.getHeight()-50);
			}
		});
		mapa.setLayout(null);
		mapa.add(map);
		menu.setLayout(null);
	
		menu.add(play);
		
		mapa.add(mainMenu);
		
		mapa.setBackground(new Color(51, 102, 153));
		menu.setBackground(Color.GRAY);
		
		panel.add(menu,"Menu");
		btnAbrirJsonCliente.setBackground(Color.DARK_GRAY);
		btnAbrirJsonCliente.setForeground(Color.WHITE);
		btnAbrirJsonCliente.setFont(font);
		
		btnAbrirJsonCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				leerClientes = new EntidadToJSON<>(new TypeToken<ArrayList<Cliente>>(){}.getType());
				leerClientes.abrirArchivo(menu);
				ingresoJsonCliente = true;
			}
		});
		btnAbrirJsonCliente.setBounds(41, 39, 162, 36);
		
		menu.add(btnAbrirJsonCliente);
		btnAbrirJsonSucursal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				leerSucursales = new EntidadToJSON<>(new TypeToken<ArrayList<Sucursal>>(){}.getType());
				leerSucursales.abrirArchivo(menu);
				ingresoJsonSucursal = true;
			}
		});
		btnAbrirJsonSucursal.setForeground(Color.WHITE);
		btnAbrirJsonSucursal.setFont(new Font("Microsoft Sans Serif", Font.PLAIN, 14));
		btnAbrirJsonSucursal.setBackground(Color.DARK_GRAY);
		btnAbrirJsonSucursal.setBounds(460, 39, 162, 36);
		
		menu.add(btnAbrirJsonSucursal);
		
		cantSucursales = new JTextField();
		cantSucursales.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				Character[] chars = {'0','1','2','3','4','5','6','7','8','9'};
				List<Character> numeros = Arrays.asList(chars);
				if(!numeros.contains(e.getKeyChar()) && e.getKeyChar() != '')
					e.setKeyChar('\0');
			}
		});
		cantSucursales.setBounds(401, 259, 158, 19);
		menu.add(cantSucursales);
		cantSucursales.setColumns(10);
		
		JLabel lblIngreseLaCantidad = new JLabel("Ingrese la cantidad de sucursales que desea abrir:");
		lblIngreseLaCantidad.setFont(font);
		lblIngreseLaCantidad.setBounds(41, 244, 324, 46); 
		menu.add(lblIngreseLaCantidad);
		panel.add(mapa,"Mapa");
		
		JButton btnExportarResultados = new JButton("Exportar resultados");
		btnExportarResultados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				leerSucursales.guardarArchivo(mapa);
			}
		});
		btnExportarResultados.setBackground(Color.DARK_GRAY);
		btnExportarResultados.setForeground(Color.WHITE);
		btnExportarResultados.setBounds(491, 10, 167, 24);
		btnExportarResultados.setFont(font);
		mapa.add(btnExportarResultados);
		
		textoMapa = new JTextField() {
		    @Override public void setBorder(Border border) {
		        // para quitar el borde del textfield
		    }
		};
		textoMapa.setForeground(Color.WHITE);
		textoMapa.setBounds(124, 12, 324, 19);
		textoMapa.setFont(font);
		textoMapa.setBackground(new Color(51, 102, 153));
		mapa.add(textoMapa);
		textoMapa.setColumns(10);
		
		getContentPane().add(panel);
		layout.show(panel,"Menu");
	}

	private Image cargarImagen(String path,int ancho, int alto ) {
		return new ImageIcon(path).getImage().getScaledInstance(ancho, alto, Image.SCALE_SMOOTH);
	}
	
	public void actionPerformed(ActionEvent event) {
	
	   Object source = event.getSource();
	
	   if (source == play) {
		   	if(cantSucursales.getText().isEmpty()) JOptionPane.showMessageDialog(null, "Ingrese un numero de sucursales!");
		    int cantidadSucursales = Integer.parseInt(cantSucursales.getText());
		    if(excepciones(cantidadSucursales)) {
		    	ArrayList<Cliente> clientes = leerClientes.getEntidades();
		    	ArrayList<Sucursal> sucursales = leerSucursales.getEntidades();
			    ArrayList<Sucursal> sucursalesAbrir =  new ArrayList<>(solver.getSucursalesThread(sucursales, clientes, cantidadSucursales));
			    int hasta = (sucursales.size()>clientes.size()) ? sucursales.size():clientes.size();
			    mapMarkerList.clear();
			    for(int i=0;i<hasta;i++) {
			    	if(i<clientes.size()) {
			    		Cliente c=clientes.get(i);
			    		mapMarkerList.add(new ImagenMarcador(c.getCoord(), cargarImagen("res/Cliente.png", 20, 20)));
			    	}
			    	if(i<sucursalesAbrir.size()) {
				    	Sucursal s=sucursalesAbrir.get(i);
				    	leerSucursales.agregarEntidad(s);
				    	mapMarkerList.add(new ImagenMarcador(s.getCoord(), cargarImagen("res/Sucursal.png", 50, 50)));
			    	}
			   }
			   map.setDisplayPosition(sucursalesAbrir.get(0).getCoord(), 9);
			   textoMapa.setText("Costo total: "+solver.getCostoTotal());
		       layout.show(panel, "Mapa");
		       setResizable(true);
		    }
	   } else if (source == mainMenu){
	       layout.show(panel, "Menu");
	       setResizable(false);
	   } 
	}
	
	private boolean excepciones(int cantidadSucursales) {
	    if(!ingresoJsonCliente) {
	    	JOptionPane.showMessageDialog(null, "Ingrese un archivo JSON de clientes!");
	    	return false;
	    }
	    if(!ingresoJsonSucursal) {
	    	JOptionPane.showMessageDialog(null, "Ingrese un archivo JSON de sucursales!");
	    	return false;
	    }
	    if(cantidadSucursales>leerSucursales.getEntidades().size() || cantidadSucursales<=0 || cantidadSucursales>leerClientes.getEntidades().size()) {
	    	JOptionPane.showMessageDialog(null, "Cantidad de sucursales incorrecta!");
	    	return false;
	    }
	    return true;
	}
	
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		Screen s = new Screen();
		
	}
}