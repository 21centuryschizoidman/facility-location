package Interfaz;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapMarkerCircle;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

public class ImagenMarcador extends MapMarkerCircle implements MapMarker {

    private Image imagen;

    public ImagenMarcador(Coordinate coord, Image imagen) {
        this(coord, 1, imagen);
    }

    public ImagenMarcador(Coordinate coord, double radio, Image image) {
        super(coord, radio);
        this.imagen = image;
    }

    @Override
    public void paint(Graphics g, Point posicion, int radio) {
        double r = this.getRadius();
        int width = (int) (this.imagen.getWidth(null) * r);
        int height = (int) (this.imagen.getHeight(null) * r);
        int w2 = width / 2;
        int h2 = height / 2;
        g.drawImage(this.imagen, posicion.x - w2, posicion.y - h2, width, height, null);
        this.paintText(g, posicion);
    }

    public Image getImagen() {
        return this.imagen;
    }

    public void setImage(Image imagen) {
        this.imagen = imagen;
    }

}