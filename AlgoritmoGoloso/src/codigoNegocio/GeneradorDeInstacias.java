package codigoNegocio;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;



public class GeneradorDeInstacias {
	ArrayList<Cliente> clientes = new ArrayList<>();
	ArrayList<Sucursal> sucursalesPosibles = new ArrayList<>();
	
	public GeneradorDeInstacias(int cantClientes, int cantSucursales) {
		for(int s=0;s<cantSucursales;s++) {
			double randomLat = ThreadLocalRandom.current().nextDouble(-34.512501, -34.500053);
			double randomLon = ThreadLocalRandom.current().nextDouble(-58.756205, -58.689246);
			
			sucursalesPosibles.add(new Sucursal("sucursal"+s, Math.floor(randomLat * 1000000) / 1000000, Math.floor(randomLon * 1000000) / 1000000));
		}
		for(int c=0;c<cantClientes;c++) {
			double randomLat = ThreadLocalRandom.current().nextDouble(-34.512501, -34.500053);
			double randomLon = ThreadLocalRandom.current().nextDouble(-58.756205, -58.689246);
			
			clientes.add(new Cliente("cliente"+c, Math.floor(randomLat * 1000000) / 1000000, Math.floor(randomLon * 1000000) / 1000000));
		}
	}
	
	public ArrayList<Cliente> getClientes(){
		return clientes;
	}
	
	public ArrayList<Sucursal> getSucursales(){
		return sucursalesPosibles;
	}
	
	public void StressTest(TriFunction<ArrayList<Sucursal>, ArrayList<Cliente>, Integer, List<Sucursal>> getSucursales, int k) {
		long inicio = System.currentTimeMillis();

		List<Sucursal> sucursales = getSucursales.apply(sucursalesPosibles, clientes, k);

		long fin = System.currentTimeMillis();

		System.out.println("Eso tomo: " + (fin - inicio) + " millisegundos");
	}

	@FunctionalInterface
	public interface TriFunction<T,U,S, R> {
		R apply(T t, U u, S s);
	}
	
	public static void main(String[] args) {
		/**GeneradorDeInstacias g = new GeneradorDeInstacias(1000, 50);
		EntidadToJSON<Cliente> clienteJSON = new EntidadToJSON<>(new TypeToken<ArrayList<Cliente>>(){}.getType());
		EntidadToJSON<Sucursal> SucursalJSON = new EntidadToJSON<>(new TypeToken<ArrayList<Sucursal>>(){}.getType());
		
		clienteJSON.agregarEntidades(g.getClientes());
		SucursalJSON.agregarEntidades(g.getSucursales());
		clienteJSON.guardarJSON("C:\\Users\\santi\\OneDrive\\Escritorio\\clientes\\clientesStress.JSON");
		SucursalJSON.guardarJSON("C:\\Users\\santi\\OneDrive\\Escritorio\\clientes\\sucursalesStress.JSON");
		*/
		GeneradorDeInstacias g = new GeneradorDeInstacias(1000, 100);
		Solver s = new Solver();
		System.out.println("Normal: ");
		g.StressTest((suc,clien,k) -> s.getSucursales(suc, clien, k), 10);
		System.out.println("Multithread: ");
		g.StressTest((suc,clien,k) -> s.getSucursalesThread(suc, clien, k), 10);
		
	}

}
