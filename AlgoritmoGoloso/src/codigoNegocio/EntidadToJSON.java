package codigoNegocio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class EntidadToJSON<T> {
	private ArrayList<T> entidades;
	private Type tipo;
	private final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	public EntidadToJSON(Type tipo) {
		entidades = new ArrayList<>();
		this.tipo = tipo;
	}
	
	public void agregarEntidad(T c) {
		entidades.add(c);
	}
	//new TypeToken<ArrayList<Cliente>>(){}.getType()
	public void agregarEntidades(ArrayList<T> entidades) {
		this.entidades = entidades;
	}
	
	public T getEntidad(int index) {
		return entidades.get(index);
	}
	
	public ArrayList<T> getEntidades(){
		return entidades;
	}
	
	public void guardarJSON(String path) {
		String json = gson.toJson(entidades);
		try
		{
			FileWriter writer = new FileWriter(path);
			writer.write(json);
			writer.close();
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	public ArrayList<T> leerJSON(String path){
		ArrayList<T> ret = null;
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(path));
			ret = gson.fromJson(br, tipo);
			br.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return ret;
	}
	public void abrirArchivo(JPanel frame) {
		  /**llamamos el metodo que permite cargar la ventana*/
		   JFileChooser file=new JFileChooser();
		   FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos JSON", "JSON", "JSON");
		   file.setFileFilter(filter);
		   file.showOpenDialog(frame);
		   /**abrimos el archivo seleccionado*/
		   File abre=file.getSelectedFile();
		   if(!getFileExtension(abre).equals("JSON"))
			   JOptionPane.showMessageDialog(null, "Ingrese un archivo JSON");
		   entidades = leerJSON(abre.getAbsolutePath());
		}
	
	public void guardarArchivo(JPanel jframe){
		JFileChooser fileChooser = new JFileChooser();
	    FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos JSON", "JSON", "JSON");
	    fileChooser.setFileFilter(filter);
		fileChooser.setDialogTitle("Guardar como: ");    
		int userSelection = fileChooser.showSaveDialog(jframe); 
		if (userSelection == JFileChooser.APPROVE_OPTION) {
		    File fileToSave = fileChooser.getSelectedFile();
		    if(getFileExtension(fileToSave).isEmpty()) guardarJSON(fileToSave.getAbsolutePath()+".JSON");
		    else guardarJSON(fileToSave.getAbsolutePath());
		}
		
	}
	
    private static String getFileExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }
}
