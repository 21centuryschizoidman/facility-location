package codigoNegocio;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

class SolverTest {
	
	@Test
	void getDistanciaTest() {
		Coordinate ungs=new Coordinate(-34.52162613189008, -58.70084256133594);
		Coordinate terrazas=new Coordinate(-34.529861715850046, -58.70087864869694);
		double solucion = 0.916; // calculada con google maps, aunque este la expresa en metros (916.06 metros para ser exacto)
		assertEquals(solucion, Solver.getDistanciaEnKm(ungs, terrazas));
	}

	@Test
	void getSucursalesTest() {
		Cliente pepe = new Cliente("pepe", -34.549962, -58.722678);
		Cliente carlos = new Cliente("carlos",-34.553907, -58.718059);		
		Sucursal peor  = new Sucursal("peor", -34.544211, -58.710834);       //la sucursal con mas distancia de los dos clientes
		Sucursal correcta = new Sucursal("correcta", -34.552487, -58.719902); //la sucursal que se encuentra entre medio de los dos clientes
		Sucursal incorrecta = new Sucursal("incorrecta", -34.548238, -58.722431);//la sucursal mas cerca de pepe pero no de carlos
		ArrayList<Cliente> clientes = new ArrayList<>();
		clientes.add(pepe);
		clientes.add(carlos);
		ArrayList<Sucursal> sucursalesTentativas = new ArrayList<>();
		sucursalesTentativas.add(correcta);
		sucursalesTentativas.add(peor);
		sucursalesTentativas.add(incorrecta);
		List<Sucursal> respuesta1Sucursal = new ArrayList<Sucursal>();
		respuesta1Sucursal.add(correcta); // la respuesta es la sucursal que se encuentra entre medio de los dos clientes
		Solver solver = new Solver();
		List<Sucursal> solucion = solver.getSucursalesThread(sucursalesTentativas, clientes, 1);
		assertTrue(respuesta1Sucursal.containsAll(solucion));
	}

	@Test
	void todasIgualesTest() {
		Cliente pepe = new Cliente("pepe", -34.549962, -58.722678);
		Cliente carlos = new Cliente("carlos", -34.553907, -58.718059);		
		Sucursal peor  = new Sucursal("peor", -34.544211, -58.710834);       
		Sucursal peor2  = new Sucursal("peor2", -34.544211, -58.710834);     
		Sucursal peor3  = new Sucursal("peor3", -34.544211, -58.710834);     
		ArrayList<Cliente> clientes = new ArrayList<>();
		clientes.add(pepe);
		clientes.add(carlos);
		ArrayList<Sucursal> sucursalesTentativas = new ArrayList<>();
		sucursalesTentativas.add(peor);
		sucursalesTentativas.add(peor2);
		sucursalesTentativas.add(peor3);
		List<Sucursal> respuesta1Sucursal = new ArrayList<Sucursal>();
		respuesta1Sucursal.add(peor);
		Solver solver = new Solver();
		List<Sucursal> solucion = solver.getSucursales(sucursalesTentativas, clientes, 1);
		assertTrue(respuesta1Sucursal.containsAll(solucion));
	}
	
	@Test
	void costoTotalTest() {
		Cliente pepe = new Cliente("pepe", -34.549962, -58.722678);
		Cliente carlos = new Cliente("carlos",-34.553907, -58.718059);		
		Sucursal peor  = new Sucursal("peor", -34.544211, -58.710834);       //la sucursal con mas distancia de los dos clientes
		Sucursal correcta = new Sucursal("correcta", -34.552487, -58.719902); //la sucursal mas cercana de los dos clientes 
		Sucursal incorrecta = new Sucursal("incorrecta", -34.548238, -58.722431);//la sucursal mas cerca de pepe pero no de carlos   
		ArrayList<Cliente> clientes = new ArrayList<>();
		clientes.add(pepe);
		clientes.add(carlos);
		ArrayList<Sucursal> sucursalesTentativas = new ArrayList<>();
		sucursalesTentativas.add(peor);
		sucursalesTentativas.add(correcta);
		sucursalesTentativas.add(incorrecta);
		Solver solver = new Solver();
		solver.getSucursales(sucursalesTentativas, clientes, 1);
		assertEquals(Solver.getDistanciaEnKm(correcta.getCoord(), pepe.getCoord())+
		Solver.getDistanciaEnKm(correcta.getCoord(), carlos.getCoord()), solver.getCostoTotal());		
		//el costo total en este caso es igual a la suma de las distancias de todos los clientes con la sucursal
	}
}
