package codigoNegocio;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.openstreetmap.gui.jmapviewer.Coordinate;

import codigoNegocio.Cliente;
import codigoNegocio.Sucursal;

public class Solver {
	private HashMap<Sucursal, Double> distanciasSucursales;
	private CopyOnWriteArrayList<Sucursal> sucursalesElegidas;
	
	public Solver() {
		distanciasSucursales = new HashMap<Sucursal, Double>(); 
	}
	
	public double getCostoTotal() {
		if(sucursalesElegidas.isEmpty())
			throw new IllegalStateException("Falta seleccionar sucursales!");
		double costoTotal = 0;
		for(Sucursal s:sucursalesElegidas)
			costoTotal += distanciasSucursales.get(s);
		return new BigDecimal(costoTotal).setScale(3, RoundingMode.HALF_UP).doubleValue(); // redondeo a 3 decimales
	}
	
    //particiona la lista de clientes y obtiene la mejor sucursal para cada particion
    public ArrayList<Sucursal> getSucursales(ArrayList<Sucursal> sucursalesPosibles, ArrayList<Cliente> clientes, int k){
    	excepciones(sucursalesPosibles, clientes, k);
    	Collections.sort(clientes, (c1, c2) -> Double.compare(c1.getCoord().getLat()+c1.getCoord().getLon(), c2.getCoord().getLat()+c2.getCoord().getLon()));    	
    	int divisiones = clientes.size()/k; 
    	int indiceL = 0;
    	int indiceR = divisiones;
    	ArrayList<Sucursal> ret = new ArrayList<>();
    	for(int i=0;i<k;i++) {	
    		ArrayList<Cliente> subCliente = new ArrayList<>(clientes.subList(indiceL, indiceR));
    		ret.add(getSucursal(sucursalesPosibles, subCliente));
    		indiceL+=divisiones;
    		indiceR+=divisiones;
    	}
    	sucursalesElegidas = new CopyOnWriteArrayList<>(ret);
    	return ret;	
    }
	
    //particiona la lista de clientes y obtiene la mejor sucursal para cada particion utilizando multithreading
    public CopyOnWriteArrayList<Sucursal> getSucursalesThread(ArrayList<Sucursal> sucursalesPosibles, ArrayList<Cliente> clientes, int k) {
    	excepciones(sucursalesPosibles, clientes, k);
    	Collections.sort(clientes, (c1, c2) -> Double.compare(c1.getCoord().getLat()+c1.getCoord().getLon(), c2.getCoord().getLat()+c2.getCoord().getLon()));    	
    	int divisiones = clientes.size()/k; 
    	int indiceL = 0;
    	int indiceR = divisiones;
    	CopyOnWriteArrayList<Sucursal> ret = new CopyOnWriteArrayList<Sucursal>();
    	ExecutorService executor = Executors.newFixedThreadPool(k);
    	for(int i=0;i<k;i++) {	
    		ArrayList<Cliente> subCliente = new ArrayList<>(clientes.subList(indiceL, indiceR));//hace la particion
    		executor.submit(new myThread(sucursalesPosibles, subCliente,ret, this));
    		indiceL+=divisiones;
    		indiceR+=divisiones;
    	}
    	executor.shutdown();
        try {
			while (executor.awaitTermination(1, TimeUnit.DAYS)) {
			    break;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	sucursalesElegidas = ret;
    	return ret;	
    }
    
    //devuelve la sucursal con menor sumatoria de distancias para los clientes dados
    public Sucursal getSucursal(ArrayList<Sucursal> sucursalesPosibles, ArrayList<Cliente> clientes){
    	HashMap<Sucursal, Double> distanciasSucursal = new HashMap<>();
    	for(int i=0;i<sucursalesPosibles.size();i++) {
    		double sumaDistancia = sumarDistancia(sucursalesPosibles.get(i), clientes);
    		distanciasSucursal.put(sucursalesPosibles.get(i),sumaDistancia);
    	}
    	Collections.sort(sucursalesPosibles, (Sucursal a1, Sucursal a2) -> Double.compare(distanciasSucursal.get(a1), distanciasSucursal.get(a2)));
    	distanciasSucursales.put(sucursalesPosibles.get(0), distanciasSucursal.get(sucursalesPosibles.get(0)));
    	return sucursalesPosibles.get(0);
    }
    //suma las distancias entre todos los clientes y una sucursal
	private static double sumarDistancia(Sucursal sucursal, ArrayList<Cliente> clientes) {
		double sumaDistancia = 0;
		for(Cliente c:clientes) {
			sumaDistancia += getDistanciaEnKm(c.getCoord(), sucursal.getCoord());
		}
		return sumaDistancia;
	}
	
    private static void excepciones(ArrayList<Sucursal> sucursalesPosibles, ArrayList<Cliente> clientes, int k) {
    	if(k>clientes.size())
    		throw new IllegalArgumentException("LA CANTIDAD DE SUCURSALES DEBE SER MENOR A LA CANTIDAD DE CLIENTES!");
    	if(k>sucursalesPosibles.size() || k<0)
    		throw new IllegalArgumentException("CANTIDAD DE SUCURSALES INCORRECTA");
    	if(sucursalesPosibles.isEmpty() || clientes.isEmpty())
    		throw new IllegalArgumentException("FALTA INGRESAR DATOS");
    }
    
    public static double getDistanciaEnKm(Coordinate c1, Coordinate c2) {
		double lon1 = c1.getLon(); 
		double lon2 = c2.getLon(); 
		double lat1 = c1.getLat(); 
		double lat2 = c2.getLat();   
    	
		double R = 6371; // Radio de la tierra en km
		double dLat = Math.toRadians(lat2-lat1); 
		double dLon = Math.toRadians(lon2-lon1); 
		double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon/2) * Math.sin(dLon/2); 
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		double d = R * c; // transformo la distancia en km
		return new BigDecimal(d).setScale(3, RoundingMode.HALF_UP).doubleValue(); // redondeo a 3 decimales
    }
}
