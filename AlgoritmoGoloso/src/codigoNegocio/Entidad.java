package codigoNegocio;

import org.openstreetmap.gui.jmapviewer.Coordinate;


public class Entidad {
	private String nombre;	
	private double lat;
	private double lon;

	public Entidad(String nombre, double lat, double lon) {
		excepcionesCoord(lat, lon);
		this.nombre = nombre;
		this.lat = lat;
		this.lon = lon;
	}

	public String getNombre() {
		return nombre;
	}

	public Coordinate getCoord() {
		return new Coordinate(lat, lon);
	}

	@Override
	public String toString() {
		return nombre+" LAT: "+lat+" LON: "+lon;
	}
	
	private void excepcionesCoord(double lat, double lon) {
		Double Dlat = new Double(lat);
		Double Dlon = new Double(lon);
		String[] div = Dlat.toString().split("\\.");
		String[] div2 = Dlon.toString().split("\\.");
		int digitosLat = div[1].length();
		int digitosLon = div[1].length();
		if(digitosLat>6 || digitosLon>6)
			throw new IllegalArgumentException("MAXIMO 6 CIFRAS DECIMALES");
		if(lat<-90 || lat>90)
			throw new IllegalArgumentException("LA LATITUD TIENE UN RANGO DESDE -90 HASTA 90");
		if(lon<-180 || lon>180)
			throw new IllegalArgumentException("LA LONGITUD TIENE UN RANGO DESDE -180 HASTA 180");
	}
	
	@Override
	public int hashCode() {
		return nombre.hashCode()+(int)lat+(int)lon;
	}
}
