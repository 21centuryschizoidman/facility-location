package codigoNegocio;

public class Sucursal extends Entidad{

	public Sucursal(String nombre, double lat, double lon) {
		super(nombre, lat, lon);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Sucursal) {
			Sucursal e = (Sucursal) o;
			return (getNombre().equals(e.getNombre()) && getCoord().equals(e.getCoord()));		
		} 
		return false;
	}
}
