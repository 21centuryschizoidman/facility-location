package codigoNegocio;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;

public class Cliente extends Entidad{

	public Cliente(String nombre, double lat, double lon) {
		super(nombre, lat, lon);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Cliente) {
			Cliente e = (Cliente) o;
			return (getNombre().equals(e.getNombre()) && getCoord().equals(e.getCoord()));		
		} 
		return false;
	}
	
}
