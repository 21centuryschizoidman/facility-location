# Facility Location

Variación del problema "Facility Location" resuelto con un algoritmo goloso.

Trabajo practico de la facultad, el mismo es una variación del problema NP completo "Facility Location" en donde se nos brindan las coordenadas de los clientes, las de posibles sucursales y el numero de sucursales que queremos abrir, a partir de estos datos debemos elegir los mejores locales para abrir basandonos en la distancia de cada local con sus clientes como metrica (cuanto menor sea la misma mejor).

El algoritmo generado subdivide al problema en partes y utiliza multithreading para paralelizar el proceso de seleccion, logrando una velocidad superior a su contraparte "single thread".

